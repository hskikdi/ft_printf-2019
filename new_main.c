#include "ft_printf.h"
#include <stdio.h>

static void run_test(void)
{
	int a =0;
	if (!a)
		;
	else;
		write(1, "a\n", 2);
}

int		main()
{
	int k1;
	int	k2;
	
	run_test();
	k1= ft_printf("test = %s", "abc");
	ft_printf("test = [%s]\n", "haris");
	k2= printf("%s", "abc");
	write(1, "\n", 1);
	if (k1 != k2)
	{
		write(1, "FAIL\n", 5);
		printf("k1 = %d et k2 = %d\n", k1, k2);
	}
	return (0);
}

/*
 * ne marche que avec plusieurs fonctions qui se suivent
 * ceci est un gros probleme
 * qui semble se situer dans ft_printf()
 * lorsqu il n y a rien derriere le %s on bug
 * tester avec dautres flags
 * plus rien ne semble marcher
 *
 * GROS SOUCIS avec le else dans le printf, revoir complete-
 * tement l'archi
 *
 *	le ft_printf pf()->stock quoi quil arrive le reréflÉchir
 *
 * */
