#include <stdio.h>
#include "ft_printf.h"

int		main()
{
	int	k1;
	int	k2;
//	ft_printf("test01234567890123456789\n");
	ft_printf("test = [%d", 42);
	printf("test = {%d\n", 42);
	ft_printf("[%+d]\n", 4242424242);
	printf("{%+d}\n", 4242424242);
	ft_printf("test = [%10.4d]\n", -42);
	printf("test = {%10.4d}\n", -42);
	ft_printf("test = [%010.4d]\n", -42);
	printf("test = {%010.4d}\n", -42);
	ft_printf("test = [%10.4d]\n", 42);
	printf("test = {%10.4d}\n", 42);
	ft_printf("test = [%10.4d]\n", 0);
	printf("test = {%10.4d}\n", 0);
	ft_printf("test = [%-10.4d]\n", -420);
	printf("test = {%-10.4d}\n", -420);
	ft_printf("test = [%10.4hd]\n", -420000);
	printf("test = {%10.4hd}\n", -420000);
	ft_printf("test = [% 10.4hhd]\n", -4200);
	printf("test = {%010.4hhd}\n", -4200);
	ft_printf("test = [%10.4x]\n", 4200);
	printf("test = {%10.4x}\n", 4200);
	ft_printf("test = [%x]\n", 4200);
	printf("test = {%x}\n", 4200);
	ft_printf("test = [%X]\n", -255);
	printf("test = {%X}\n", -255);
	ft_printf("test = [%hx]\n", -4);
	printf("test = {%hx}\n", -4);
	ft_printf("test = [%hhx]\n", -4);
	printf("test = {%hhx}\n", -4);
	ft_printf("test = [%hhd]\n", -2555);
	printf("test = {%hhd}\n", -2555);
	ft_printf("test = [%hhu]\n", -2555);
	printf("test = {%hhu}\n", -2555);
	ft_printf("test = [%hd]\n", -2555);
	printf("test = {%hd}\n", -2555);
	ft_printf("test = [%hhx]\n", -2555);
	printf("test = {%hhx}\n", -2555);
	ft_printf("test = [%x]\n", -4);
	printf("test = {%x}\n", -4);
	ft_printf("test = [%hhx]\n", -4);
	printf("test = {%hhx}\n", -4);
	ft_printf("test = [%hu]\n", -4000);
	printf("test = {%hu}\n", -4000);
	ft_printf("test = [%*.*hu]\n", 15, 15, -4000);
	printf("test = {%*.*hu}\n", 15,15,-4000);
	ft_printf("test = [%0--10d]\n", 42);
	printf("test = {%0--10d}\n", 42);
	ft_printf("test = [%+010d]\n", -42);
	printf("test = {%+010d}\n", -42);
	ft_printf("test = [%+10.15d]\n", 42);
	printf("test = {%+10.15d}\n", 42);
	ft_printf("test = [%d]\n", -2147483648);
	printf("test = {%d}\n", -2147483648);

/*	DEBUT DES STR*/

	
	ft_printf("test = [%.-5s]\n", "haris");
	printf("test = {%.-5s}\n", "haris"); // a regler asap
	ft_printf("test = [%s]\n", "haris");
	printf("test = {%s}\n", "haris");
	ft_printf("test = [%5.3s]\n", "haris");
	printf("test = {%5.3s}\n", "haris");
	ft_printf("test = [%-05.3s]\n", "haris");
	printf("test = {%-05.3s}\n", "haris");
	ft_printf("test = [%010s]\n", "haris");
	printf("test = {%010s}\n", "haris");
	ft_printf("test = [%s]\n", NULL);
	printf("test = {%5s}\n", NULL);

	/*DEBUT DES CHAR*/

	k1 =	ft_printf("test = [%c]\n", 'a');
k2 = 	printf("test = {%c}\n", 'a');
if (k1 != k2)
	printf("k 1 != k2\n");
k1 =	ft_printf("test = [%c]\n", 0);
k2= 	printf("test = {%c}\n", 0);
if (k1 != k2)
	printf("k 1 != k2\n");
k1 = 	ft_printf("test = [%10.4c]\n", 'a');
k2 =	printf("test = {%10.5c}\n", 'a');
if (k1 != k2)
	printf("k 1 != k2\n");
k1 =	ft_printf("test = [%0-10.3c]\n", 'a');
k2 = 	printf("test = {%0-10.3c}\n", 'a');
if (k1 != k2)
	printf("k 1 != k2\n");
	
/* pointeur et bases*/
	k1 = ft_printf("testpointeur = [%p]\n", 0);
	k2 = printf("testpointeur = {%p}\n", 0);
	k1 = ft_printf("testpointeur = [%hx]\n", 0);
	k2 = printf("testpointeur = {%hx}\n", 0);
	k1 = ft_printf("testpointeur = [%10p]\n", 0);
	k2 = printf("testpointeur = {%10p}\n", 0);
	k1 = ft_printf("testpointeur = [%5.3p]\n", 0);
	k2 = printf("testpointeur = {%5.3p}\n", 0);
	k1 = ft_printf("testpointeur = [%5.3p]\n", 42);
	k2 = printf("testpointeur = {%5.3p}\n", 42);
	k1 = ft_printf("testpointeur = [%10p]\n", -1);
	k2 = printf("testpointeur = {%10p}\n", -1);
	k1 = ft_printf("testpointeur = [%x]\n", -1);
	k2 = printf("testpointeur = {%x}\n", -1);
	k1 = ft_printf("testpointeur = [%#x]\n", -1);
	k2 = printf("testpointeur = {%#x}\n", -1);
	k1 = ft_printf("testpointeur = [%#o]\n", -1);
	k2 = printf("testpointeur = {%#o}\n", -1);
	k1 = ft_printf("testpointeur = [%#5o]\n", 0);
	k2 = printf("testpointeur = {%#5o}\n", 0);
	k1 = ft_printf("testpointeur = [%0#5x]\n", 0);
	k2 = printf("testpointeur = {%0#5x}\n", 0);
	ft_printf("test = [%010.4d]\n", -42);
	printf("test = {%010.4d}\n", -42);
	ft_printf("test = [%010d]\n", -42);
	printf("test = {%010d}\n", -42);
	ft_printf("test = [%010d]\n", -42);
	printf("test = {%010d}\n", -42);
	ft_printf("test = [%10d]\n", -42);
	printf("test = {%10d}\n", -42);
	ft_printf("test = [%010.5d]\n", -42);
	printf("test = {%010.5d}\n", -42);
	ft_printf("test = [%s]\n", NULL);
	printf("test = {%5s}\n", NULL);
/*	printf("test= {%#10x}\n", 10);
	printf("test= {%10p}\n", 10);
	printf("test= {%10p}\n", 10);
	printf("test= {%p}\n", 0);

*/
}
