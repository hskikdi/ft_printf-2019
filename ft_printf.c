#include "ft_printf.h"

t_print		*pf()
{
	static t_print	p = {{0}, NULL,  0, 0, 1, 0, 0};

	if (!p.start)
	{
		p.start = 1;
		ft_bzero(p.buff, BUFFER_SIZE);
	}
	return (&p);
}

t_parse		*parse(void)
{
	static t_parse p = {0, 0, -1, 0, 0, 0, 10, 0};

	return (&p);

}

static void		pf_parse(va_list params)
{
	parse_flags();
	parse_width(params);
	parse_preci(params);
	parse_modif_and_speci();
//	printf("apres parse : [%s]\n", pf()->format + pf()->index);
	return ;
}

int			ft_printf(const char *s, ...)
{
	va_list		params;

	va_start(params, s);
	pf()->format = s;
	while (s[pf()->index])
	{
		if (s[pf()->index] == '%')
		{
			pf()->index++;
			if (!s[pf()->index])
				break ;
			pf_parse(params);
			pf_switch(params);
		}
		else
			pf_stock_one(s[pf()->index]);
		if (s[pf()->index])
			pf()->index++;
	}
	va_end(params);
	return (pf_print());
}

/* reinitialiser les structures avant de return ? ou apres print */
