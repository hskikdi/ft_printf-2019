

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include "libft.h"
# include <sys/stat.h>
# include <sys/types.h>
# include <fcntl.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <stdarg.h>
# define BUFFER_SIZE 64U
# define FLAG_LESS      (1)
# define FLAG_PLUS      (1 << 1)
# define FLAG_HASHTAG   (1 << 2)
# define FLAG_ZERO      (1 << 3)
# define FLAG_SPACE     (1 << 4)
# define HEXASTRING		"0123456789abcdef"

typedef struct		s_print
{
	char			buff[BUFFER_SIZE + 1];
	const char		*format;
	int				start;
	int				ret;
	int				fd;
	int				buf_len;
	int				index;
}					t_print;

typedef struct		s_parse
{
	unsigned int	opt;
	int				width;
	int				preci;
	int				modifier;
	int				speci;
	int				neg;
	int				base;
	int				maj;
}					t_parse;

int					ft_printf(const char *s, ...);
void				init_print(t_print *p);
t_print				*pf(void);
t_parse				*parse(void);
//void				pf_parse(void);
void				pf_stock_one(char c);
int					pf_print(void);
void				parse_flags(void);
void				parse_width(va_list params);
void				parse_preci(va_list params);
void				parse_modif_and_speci(void);
void				pf_itoa(unsigned long long nbr);
void				fill_preci(int size_nbr);
void				fill_width(int size_nbr, unsigned long long n);
int					size_nbr(unsigned long long n);
void				field(char c, int n);
void				pf_int(unsigned long long n);
void				pf_int_flag_less(unsigned long long n);
void				conv_and_handle_int(va_list params);
void				conv_and_handle_next(va_list params);
void				re_init(void);
void				pf_string(va_list params);
void				preci_and_str(char *str);
void				width_str(int str_len);
//void				pf_char(va_list params);
void				pf_char(char c);
void				pf_conv_char(va_list params);
int					hash_size(unsigned long long n);
int					sign_size(void);
void				pf_switch(va_list params);
int					spec_zero(unsigned long long n);

#endif
