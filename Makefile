# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/12 17:22:58 by hskikdi           #+#    #+#              #
#    Updated: 2020/02/25 03:56:09 by hskikdi          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

OBJ=$(addsuffix .o,		srcs/init srcs/pf_char srcs/pf_fill \
						srcs/pf_int_conversion srcs/pf_integer srcs/pf_itoa \
						srcs/pf_parse srcs/pf_stock srcs/pf_string \
						srcs/pf_switch srcs/pf_utiles_int ft_printf)

NAME=libftprintf.a

#FLAG= -g -fsanitize=address -Wall -Werror -Wextra
FLAG= -Wall -Werror -Wextra

all: $(NAME)

$(NAME): $(OBJ)
	make -C libft
	cp libft/libft.a libftprintf.a
	ar -rsc $(NAME) $(OBJ)
	ranlib $(NAME)

%.o: %.c
	gcc $(FLAG) -c -o $@ $< -Iincludes -Ilibft

clean:
	make clean -C libft
	rm -rf $(OBJ)

fclean: clean
	rm -f $(NAME)
	make fclean -C libft

re: fclean all
