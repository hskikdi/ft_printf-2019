#include "ft_printf.h"

void	pf_stock_one(char c)
{
	if (pf()->buf_len == BUFFER_SIZE)
	{
		pf()->ret += write(1, pf()->buff, BUFFER_SIZE);
//		ft_bzero((pf()->buff), BUFFER_SIZE);
		(pf()->buff)[0] = c;
		pf()->buf_len = 1;
	}
	else
	{
		(pf()->buff)[pf()->buf_len] = c;
		pf()->buf_len++;
		(pf()->buff)[pf()->buf_len] = (char)0;
	}
//	pf()->ret += write(1, &c, 1);
}

int		pf_print(void)
{
	int ret;

	ret = pf()->ret;
	ret += write(pf()->fd, pf()->buff, pf()->buf_len);
//	if (pf()->buf_len != BUFFER_SIZE)
//	{
		pf()->buf_len = 0;
		ft_bzero(pf()->buff, BUFFER_SIZE);
		pf()->index = 0;
//	}
	pf()->ret = 0;
	//ret = pf()->ret;
//	pf()->ret = 0;
	return (ret);
}
