#include "ft_printf.h"

void	parse_flags(void)
{
//	int	i;
//	char *format;

//	format = pf()->format;
//	i = pf()->index;
	while (ft_strchr("-+ #0", pf()->format[pf()->index]) && pf()->format[pf()->index])
	{
		
		if (pf()->format[pf()->index] == '-')
			parse()->opt |= FLAG_LESS;
		else if (pf()->format[pf()->index] == '+')
			parse()->opt |= FLAG_PLUS;
		else if (pf()->format[pf()->index] == ' ')
			parse()->opt |= FLAG_SPACE;
		else if (pf()->format[pf()->index] == '#')
			parse()->opt |= FLAG_HASHTAG;
		else if (pf()->format[pf()->index] == '0')
			parse()->opt |= FLAG_ZERO;
		pf()->index++;
	}
}

void	parse_width(va_list params)
{
	int		nb;
//	int		i;
//	char 	*format;

//	format = pf()->format;
//	i = pf()->index;
	if (pf()->format[pf()->index] == '*')
	{
		/*a coder, ajouter le va ici*/
		parse()->width = va_arg(params, int);
		if (parse()->width < 0)
		{
			parse()->opt |= FLAG_LESS;
			parse()->width *= -1;
		//	printf("widht = %d\n", parse()->width);
		}
		pf()->index++;
//		return ;
	}
	if (ft_isdigit(pf()->format[pf()->index]))
	{
		nb = ft_atoi(pf()->format + pf()->index);
		while (ft_isdigit(pf()->format[pf()->index]))
			pf()->index++;
		parse()->width = nb;
	}
}

void	parse_preci(va_list params)
{
	int		nb;

	if (pf()->format[pf()->index] != '.'/* || !pf()->format[pf()->index]*/)
	{
		return ;
	}
	pf()->index++;
	if (pf()->format[pf()->index] == '*')
	{
		parse()->preci = va_arg(params, int);
		if (parse()->preci < 0)
		{
			parse()->preci = -1;
	//		parse()->opt |= FLAG_LESS;
		}
		pf()->index++;
		return ;
	}
	if (ft_isdigit(pf()->format[pf()->index]))
	{
		nb = ft_atoi(pf()->format + pf()->index);
		while (ft_isdigit(pf()->format[pf()->index]))
			pf()->index++;
		parse()->preci = nb;
	}
	else
		parse()->preci = 0;
}

void	parse_modif_and_speci(void)
{
	//int		i;
	//char 	*format;

	//format = pf()->format;
//	i = pf()->index;
//	if (ft_strchr("hljzt"), format[i])
//	printf("-->%c<--\n", pf()->format[pf()->index]);
	char *str = NULL;
	if (!pf()->format[pf()->index])
		return ;
	if ((str = ft_strchr("hljzt", pf()->format[pf()->index])))
	{
		parse()->modifier = str[0];
		if (str[0] == 'h' || str[0] == 'l')
			parse()->modifier -= (pf()->format[pf()->index + 1] == str[0]) ?
				32 : 0;
		//parse()->modifier <= 'Z' ? pf()->index += 2 : pf()->index++;
		pf()->index += (parse()->modifier <= 'Z') ? 2 : 1;
	}
	if ((str = ft_strchr("sSpdDioOuUxXcC%", pf()->format[pf()->index])))
	{
		parse()->speci = str[0];
	//	pf()->index++;
	}
}
