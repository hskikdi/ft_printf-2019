#include "ft_printf.h"

void	preci_and_str(char *str)
{
	int	i;

	i = 0;
	if (parse()->preci == -1)
	{
		while (str[i])
			pf_stock_one(str[i++]);
		return ;
	}
	while (i < parse()->preci && str[i] && parse()->preci > -1)
		pf_stock_one(str[i++]);
}

void	width_str(int str_len)
{
	int		size;

	size = (parse()->preci < str_len && parse()->preci != -1) ? parse()->preci : str_len;
	if (parse()->opt & FLAG_ZERO && !(parse()->opt & FLAG_LESS))
	{
		field('0', parse()->width - size);
	}
	else
		field(' ', parse()->width - size);
}

static void	pf_string_left(char *str)
{
	int str_len;

	str_len = ft_strlen(str);
	preci_and_str(str);
	width_str(str_len);
}

void	pf_string(va_list params)
{
	char	*str;
	int		str_len;

	str = va_arg(params, char *);
	if (parse()->opt & FLAG_LESS)
	{
		if (!str)
			return (pf_string_left("(null)"));
		return (pf_string_left(str));
	}
	if (!str)
	{
		str_len = ft_strlen("(null)");
		width_str(str_len);
		return (preci_and_str("(null)"));
	}
	str_len = ft_strlen(str);
	width_str(str_len);
	preci_and_str(str);
}
