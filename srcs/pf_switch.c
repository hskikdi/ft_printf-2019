#include "ft_printf.h"

void	pf_switch(va_list params)
{
	if (!parse()->speci)
	{
		;
	}
	else if (ft_strchr("ouxdiXp", parse()->speci))
	{
		conv_and_handle_int(params);
	}
	else if (ft_strchr("sS", parse()->speci))
	{
		pf_string(params);
	}
	else if (parse()->speci == 'c' || parse()->speci == '%')
	{
		pf_conv_char(params);
	}
	re_init();
}
