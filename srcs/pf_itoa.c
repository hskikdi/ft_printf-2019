/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/29 15:45:17 by hskikdi           #+#    #+#             */
/*   Updated: 2020/02/25 03:22:03 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		is_upper(char c)
{
	if (c >= 'a' && parse()->maj)
		return (32);
	return (0);
}

static void		core_itoa(char *str, unsigned long long  n, int *index)
{
	if (n >= (unsigned long long)parse()->base)
		core_itoa(str, n / parse()->base, index);
	pf_stock_one(HEXASTRING[n % parse()->base] - is_upper(HEXASTRING[n % parse()->base]));
}
/* remplacer par un pf_stock_one, sinon le buffer pourrait deborder*/
/*remplacer les 10 par parse()->base, avec un systeme de tableau
 * ajouter une variable parse()->maj, pour les xX
 * creer une fonction, is_upper qui renvoie 32 si parse()->maj existe*/

void			pf_itoa(unsigned long long nbr)
{
//	unsigned int	n;
//	int				i;

//	i = 0;
//	n = nbr;
//	n = (nbr >= 0) ? nbr : -nbr;
//	if (nbr < 0)
//		pf_stock_one('-');
	core_itoa((pf()->buff), nbr, &(pf()->buf_len));
//	printf("[%s]]\n", pf()->format);/*ceci est un bug de fou*/
}
