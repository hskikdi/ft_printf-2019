#include "ft_printf.h"

void	pf_char(/*va_list params*/char c)
{
//	char c;

//	c = (parse()->speci == 'c') ? (char)va_arg(params, int) : '%';
	if (parse()->opt & FLAG_LESS)
	{
		pf_stock_one(c);
		field(' ', parse()->width - 1);
		return ;
	}
	if (parse()->opt & FLAG_ZERO)
		field('0', parse()->width - 1);
	else
		field(' ', parse()->width - 1);
	if (c != -1)
		pf_stock_one(c);
}

/*
 * rajouter un parametre char ?
 * */

void	pf_conv_char(va_list params)
{
	char c;

	c = (parse()->speci == 'c') ? (char)va_arg(params, int) : '%';
	pf_char(c);

}
