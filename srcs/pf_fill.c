
#include "ft_printf.h"

int		hash_size(unsigned long long n)
{
	if (parse()->opt & FLAG_HASHTAG || parse()->speci == 'p')
	{
		if (ft_strchr("xXo", parse()->speci) && n)
		{
			if (ft_strchr("xX", parse()->speci))
				return (2);
			return (1);
		}
		else if (parse()->speci == 'p')
		{
			return (2);
		}
	}
	return (0);
}

void	fill_preci(int size_nbr)
{
	if (parse()->preci != -1)
	{
		if (parse()->preci > size_nbr)
			field('0', parse()->preci - size_nbr);
	}
}

void	fill_width(int size_nbr, unsigned long long n)
{
	int		total_size;

	total_size = (parse()->preci > size_nbr) ? parse()->preci : size_nbr;
	total_size += sign_size() + hash_size(n);/*ne gere pas le flag plus, creer une fonction qui renvoie 1 si le 
	signe est present, ou faire une ternaire degueu , GERER ICI LE FLAG_SPACE(prendre en compte la taille)?*/
	if (parse()->width < total_size)
		return ;
//	total_size -= (parse()->preci) ? 1 : 0;
	if (parse()->opt & FLAG_ZERO && !(parse()->opt & FLAG_LESS) && parse()->preci == -1)
		return (field('0', parse()->width - total_size));
	field(' ', parse()->width - total_size);
//	write(1, "OK\n", 3);
//	ft_putnbr_fd(1, (parse()->width));
//	write(1, "\n", 1);
//	printf("[[%d]]\n", parse()->width);
//	printf("[[%d]]\n", total_size);

}

int		size_nbr(unsigned long long n)
{
	int		ret;

	ret = 1;
	while ((n = n / parse()->base))
	{
		ret++;
	}
//	printf("ret dans size_nbr = %d\n",ret);
//	printf("hash_size(n) = %d, size_nbr = %d\n", hash_size(n), ret);
	return (ret);
}

void	field(char c, int n)
{
	int nb;

	nb = n;
	while (nb-- > 0)
	{
		pf_stock_one(c);
	}
}

/* faire en sorte que ca soit bon pour toutes les casts, gerer le reste des flags comme l indentation
 * a gauche
 * gerer toutes les bases dun coup
 * remplacer 10 par base, et gerer les negs en dehors de pf_itoa
 * */
