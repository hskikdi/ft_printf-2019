#include "ft_printf.h"

unsigned long long 	conv_int_to_uint(long long n)
{
	unsigned long long ret;

	if (ft_strchr("Xxupo", parse()->speci))
	{
		ret = n;
		return (ret);
	}
	if (n < 0 && (parse()->neg = 1))
		ret = -n;
	else
		ret = n;
	return (ret);
}

void	conv_and_handle_next(va_list params)
{
	int					n;
	unsigned long long	u_n;

	u_n = 0;
	if (parse()->modifier == 'h')
	{
		n = (short)va_arg(params, int);
	//	if (n < 0 && (parse()->neg = 1))
	//		n = -n;
		u_n = (unsigned short)conv_int_to_uint((short)n);
	//	short int test = (short int)u_n;
//		printf("--[%u]--\n", test);
	}
	else if (parse()->modifier == 'H')
	{
		n = (char)va_arg(params, int);
	//	if (n < 0 && (parse()->neg = 1))
	//		n = -n;
		u_n = (unsigned char)conv_int_to_uint((char)n);
	}
	else
	{
	//	if (parse()->speci == 'u')
	//		return (pf_int(va_arg(params, unsigned int)));
		if (parse()->speci == 'p')
			return (pf_int((unsigned long long)va_arg(params, char *)));
		n = va_arg(params, int);
	//	if (n < 0 && (parse()->neg = 1))
	//		u_n = -n;
	//	else
	//		u_n = n;
		u_n = (unsigned int)conv_int_to_uint(n);
//		pf_int((unsigned long)u_n);
//		return ;
	}
//	pf_int((unsigned long long)n);
	pf_int(u_n);
}

void	conv_and_handle_int(va_list params)
{
	long				n;
	unsigned long long	u_n;

	if (parse()->modifier == 'l')
	{
		n = va_arg(params, long);
//		u_n = (n < 0) ? -n : n;
//		if (n < 0)
//			parse()->neg = 1;
		u_n = conv_int_to_uint(n);
		pf_int(u_n);
	}
	else if (parse()->modifier == 'L')
	{
		n = va_arg(params, long long);
	//	u_n = (n < 0) ? -n : n;
	//	if (n < 0)
	//		parse()->neg = 1;
		u_n = conv_int_to_uint(n);
		pf_int(u_n);
	}
	else
		conv_and_handle_next(params);
}

/*creer une fonction qui converti un int en uint, qui gere le parse()->neg et les flags uxX*/
