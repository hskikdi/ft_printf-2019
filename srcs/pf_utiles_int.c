#include "ft_printf.h"

int		sign_size(void)
{
	if (ft_strchr("xXuop", parse()->speci))
		return (0);
	if (parse()->neg)
		return (1);
	else if (parse()->opt & FLAG_SPACE)
		return (1);
	else if (parse()->opt & FLAG_PLUS)
		return (1);
	return (0);
}

static int		spec_zero_p(void)
{
	parse()->preci = -1;
	if (parse()->opt & FLAG_LESS)
	{
		preci_and_str("0x");
		width_str(2);
		return (1);
	}
	width_str(2);
	preci_and_str("0x");
	return (1);


}

int		spec_zero(unsigned long long n)
{
	if (n || parse()->preci)
		return (0);
	if (parse()->speci == 'p')
		return (spec_zero_p());
	if (parse()->width)
	{
		if (parse()->opt & FLAG_PLUS)
			pf_char('+');
		else
			pf_char(' ');
	}
	else
	{
		if (parse()->speci == 'o' && parse()->opt & FLAG_HASHTAG)
		{
			pf_char('0');
			return (1);
		}
		pf_char(parse()->opt & FLAG_PLUS ? '+' : -1);
	}
	return (1);
}
