#include "ft_printf.h"

static void	handle_hash(unsigned long long n)
{
	if (parse()->opt & FLAG_HASHTAG || parse()->speci == 'p')
	{
		if (ft_strchr("xXo", parse()->speci) && n)
		{
			pf_stock_one('0');
			if (ft_strchr("xX", parse()->speci))
				pf_stock_one('x' - parse()->maj);
		}
		else if (parse()->speci == 'p')
		{
			pf_stock_one('0');
			pf_stock_one('x');
		}
	}
}

static void	handle_sign(void)
{
	if (ft_strchr("xXuop", parse()->speci))
		return ;
	if (parse()->neg)
		pf_stock_one('-');
	else if (parse()->opt & FLAG_PLUS)
		pf_stock_one('+');
	else if (parse()->opt & FLAG_SPACE)
		pf_stock_one(' ');
}

static void	handle_base(void)
{
	if (parse()->speci == 'x' || parse()->speci == 'X' || parse()->speci == 'p')
	{
		parse()->base = 16;
		parse()->maj = (parse()->speci == 'X') ? 32 : 0;
		parse()->neg = 0;
	}
	if (parse()->speci == 'o')
	{
		parse()->neg = 0;
		parse()->base = 8;
		parse()->neg = 0;
		if (parse()->opt & FLAG_HASHTAG && parse()->preci > -1)
			parse()->preci--;
	}
	if (parse()->base != 10)
	{
		parse()->opt &= ~FLAG_PLUS;
	}
	/*mettre le parse()->neg a zero*/
}


void	pf_int(unsigned long long n)
{
	int				size;
	
	handle_base();
	if (spec_zero(n))
	{
		return ;
	}
	if (parse()->opt & FLAG_LESS)
	{
		pf_int_flag_less(n);
		return ;
	}
	size = size_nbr(n);
	if (parse()->opt & FLAG_ZERO && parse()->preci == -1)
	{
		handle_sign();
		handle_hash(n);
		fill_width(size, n);
	}
	else
	{
		fill_width(size, n);
		handle_sign();
		handle_hash(n);
	}
	fill_preci(size);
	pf_itoa(n);
}
/*
 * pour le %p et %#x ajouter des fonctions qui gere ca *
 * et les integrer dans 
 * */
void	pf_int_flag_less(unsigned long long n)
{
	int				size;

//	unsgnd_n = (n < 0) ? -n : n;
	size = size_nbr(n);
	handle_sign();
	handle_hash(n);
	fill_preci(size);
	pf_itoa(n);
	fill_width(size, n);
}

/*
 * rendre generique pour tout les casts differents (donc itoa et les autres 
 * fonctions doivent manipuler des uint_max
 * rendre generique pour gerer aussi les bases differentes de 10
 * gerer les cas en fonction des flags (flag_less, indenter a gauche etc)
 * traiter la gestions des nb negatifs en dehors de itoa
 * */
